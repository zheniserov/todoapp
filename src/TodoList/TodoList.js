import React, { Component } from "react";

import "./TodoList.scss";
import deleteImage from "../img/delete.svg";
import editImage from "../img/edit.svg";
import checkImage from "../img/checked.svg";
import backImage from "../img/return.svg";

export default class TodoList extends Component {
  state = {
    inputValue: ""
    //editing: true
  };

  toggleEditing = event => {
    this.setState({ inputValue: event.target.value });
  };

  handleEdit = event => {
    event.preventDefault();
    const editedTodo = {
      text: this.state.inputValue,
      done: false
    };
    this.props.editTodo(editedTodo);
    this.setState({ inputValue: "" });
  };

  render() {
    const { list, deleteTodo, hideTodo, done } = this.props;
    return (
      <div className="App__todo">
        {list.map((todo, index) => (
          <div key={index}>
            <div
              className={`App__todo__content ${
                todo.done ? "App__todo__content--done" : ""
              }`}
            >
              <span>{todo.text}</span>
              <div className="App__actions">
                <button
                  onClick={() => {
                    done(index);
                  }}
                >
                  <img
                    className="App__actions--icons"
                    alt="checked"
                    src={checkImage}
                  />
                </button>
                <button
                  onClick={() => {
                    hideTodo(index);
                  }}
                >
                  <img
                    className="App__actions--icons"
                    alt="edit"
                    src={editImage}
                  />
                </button>
                <button
                  onClick={() => {
                    deleteTodo(index);
                  }}
                >
                  <img
                    className="App__actions--icons"
                    alt="delete"
                    src={deleteImage}
                  />
                </button>
              </div>
            </div>

            <div className="App__edit__content">
              <form className="Edit__form" onSubmit={this.handleEdit}>
                <input
                  className="App__edit"
                  value={this.state.inputValue}
                  onChange={this.toggleEditing}
                  required
                />
                <div className="App__actions">
                  <button>
                    <img
                      className="App__edit--icons"
                      alt="checked"
                      src={checkImage}
                    />
                  </button>
                  <button
                    onClick={() => {
                      hideTodo(index);
                    }}
                  >
                    <img
                      className="App__edit--icons"
                      alt="back"
                      src={backImage}
                    />
                  </button>
                </div>
              </form>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
