import React, { Component } from "react";

import "./AddTodo.scss";

export default class AddTodo extends Component {
  state = {
    inputValue: "Todo"
  };

  onInputChange = event => {
    this.setState({ inputValue: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();

    const newTodo = {
      text: this.state.inputValue,
      done: false
    };
    this.props.addAction(newTodo);
    this.setState({ inputValue: "" });
  };

  render() {
    const { inputValue } = this.state;

    return (
      <form className="App__form" onSubmit={this.handleSubmit}>
        <input
          className="App__input"
          value={inputValue}
          name="todo"
          onChange={this.onInputChange}
          required
        />
        <button className="App__form__button">Add task</button>
      </form>
    );
  }
}
