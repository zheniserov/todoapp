import React, { Component } from "react";
import AddTodo from "./AddTodo/AddTodo";
import TodoList from "./TodoList/TodoList";

import "./App.scss";

var num;

class App extends Component {
  state = {
    todos: []
  };

  addTodo = todo => {
    this.setState(currentState => ({
      todos: [...currentState.todos, todo]
    }));
  };

  deleteToDo = index => {
    this.setState(state => {
      state.todos.splice(index, 1);
      return { todos: state.todos };
    });
  };

  toggleDone = index => {
    this.setState(state => {
      const { todos } = state;
      todos[index].done = !todos[index].done;

      return { todos };
    });
  };

  editToDo = todo => {
    this.setState(state => {
      state.todos.splice(num, 1, todo);
      console.log(state.todos);
      this.hideToDo(num);
    });
  };

  hideToDo = index => {
    const todo = document.getElementsByClassName("App__todo__content");
    const edit = document.getElementsByClassName("App__edit__content");

    if (todo[index].style.display === "none") {
      todo[index].style.display = "flex";
      edit[index].style.display = "none";
    } else {
      todo[index].style.display = "none";
      edit[index].style.display = "flex";
    }
    num = index;
  };

  render() {
    const { todos } = this.state;
    return (
      <header className="App-header">
        <div className="App__content">
          <div className="App">
            <AddTodo addAction={this.addTodo} />
            <TodoList
              list={todos}
              deleteTodo={this.deleteToDo}
              hideTodo={this.hideToDo}
              editTodo={this.editToDo}
              done={this.toggleDone}
            />
          </div>
        </div>
      </header>
    );
  }
}

export default App;
